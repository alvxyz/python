# Sort is mehotd for sorting

// Sorting dari urutan terkecil hingga terbesar
x = [5, 6, 1, 2, 3, 7, 9, 10]
x.sort()
print(x)

// Melakukan pengurutan pada kata (huruf alphabet)
y = ['ants', 'cats', 'dogs', 'badgers', 'elephants']
y.sort()
print(y)

# sort can be reverse from big to small
x.sort(reverse=True)
print(x)

# Sort Can't sorting number and letter
# Metode sort menggunakan urutan ASCII,
# sehingga nilai huruf kapital (uppercase) akan lebih
# dahulu dibandingkan huruf kecil (lowercase). Contohnya:

m = ['Alice', 'ants', 'Bob', 'badgers', 'Carol', 'cats']
m.sort()
print(m)

spam = ['a', 'z', 'A', 'Z']
spam.sort(key=str.lower)
print(spam)
